# Studying Compositional Data using Bregman Divergence

Compositional data consists of a collection of nonnegative data
that sum to a constant value. Examples of compositional data include
histograms and proportions, which appear in many practical applications.
Since the parts of the collection are statistically dependent,
many standard tools cannot be directly applied. Instead, compositional data must
be first transformed before analysis.

We study machine learning problems on compositional data using the scaled Bregman
theorem, which was recently proved. The scaled Bregman theorem relates
the perspective transform of a Bregman divergence to the Bregman divergence
of a perspective transform and a remainder conformal divergence.
This equality enables us to transform machine learning problems on compositional
data to new problems which are easier to optimise. We apply this to
principal component analysis and linear regression, and show promising
results on microbiome data.
